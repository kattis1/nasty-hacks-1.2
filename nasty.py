import sys

counter = 0
for line in sys.stdin:
    if counter == 0:
        max_num = int(line)
    elif counter <= max_num:
        logs = line.split(' ')
        if float(logs[0]) < (float(logs[1]) - float(logs[2])):
            print("advertise")
        elif float(logs[0]) > (float(logs[1]) - float(logs[2])):
            print("do not advertise")
        else:
            print("does not matter")
        
    counter+=1
    if counter > max_num:
        break